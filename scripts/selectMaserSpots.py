from itertools import chain
from plotMaserSpots import show_clusters

import argparse
import numpy

from cli import selectMS_cli as cli_
from fileio import read_maser_measurements as read_file
from fileio import read_selected_clusters as read_previous


# re-associate selected (x,y) with measurements over epochs
def associate_selection(x, y, inputdata):
    """
    Re-associate selected (x,y) spots with maser measurements
    over all input epochs

    :param x: x-coords of all selected maser spots
    :param y: y-coords of all selected maser spots
    :param inputdata: maser measurements read from input file
    :returns: dict {epoch: [measurements of selected spot]}
    """
    data_selected = numpy.vstack((x, y)).T
    n_epochs = len(inputdata)
    radius = lambda x, y: numpy.sqrt(x*x + y*y)

    def dict_():
        pmfitment = {}
        for epoch in range(n_epochs):
            pmfitment['epoch_{}'.format(epoch)] = []
        return pmfitment

    pmfitment = dict_()
    for cnt, coord in enumerate(data_selected):
        [x_, y_] = coord
        coords = []
        # find in which epoch is data point
        for epoch, data in enumerate(inputdata):
            x = data[:, 1]
            y = data[:, 3]
            idx = radius(x_-x, y_-y).argmin()
            coords.append([x[idx], y[idx], idx])
        distances = [radius(x_-coord[0], y_-coord[1]) for coord in coords]
        epoch = numpy.argmin(distances)
        pmfitment['epoch_{}'.format(epoch)].append(
            inputdata[epoch][coords[epoch][-1]])
    return pmfitment


# assume data input [#epochs, [Vr, X, Xerr, Y, Yerr]]
def group_vlsr(data, threshold=0.1):
    """
    Group the data to vlsr +- 0.1 (threshold)

    :param data: maser astrometry data measurements
                 [#clusters, #epochs, [Vr, X, Xerr, Y, Yerr]]
    :param threshold: group maser spots that is close in Vr
    :returns: block of text with per line format
                 [Vr, Vgrp]
    """
    vlsr = []
    for cluster in data:
        for epoch in range(len(cluster)):
            if not len(cluster[epoch]):  # no data for this epoch
                continue
            cluster[epoch] = numpy.asarray(cluster[epoch])
            vlsr.extend(cluster[epoch][:, 0])
    vlsr = numpy.unique(vlsr)
    grp_cnt = 0
    vlsr_ = -1*numpy.ones((len(vlsr), 2))
    vlsr_[:, :-1] = vlsr[:, numpy.newaxis]
    for idx, val in enumerate(vlsr):
        if vlsr_[idx, 1] < 0:
            group_idx = numpy.abs(val-vlsr) < threshold
            vlsr_[group_idx, 1] = grp_cnt
            grp_cnt += 1
    return vlsr_


# convert input data line to output data format
def data2text(data, threshold=0.1):
    """
    Converts input data lines to output data strings

    :param data: maser astrometry data measurements
                 [#clusters, #epochs, [Vr, X, Xerr, Y, Yerr]]
    :param threshold: group maser spots that is close in Vr
    :returns: block of text with per line format
              'V\tX\tXerr\tY\tYerr\tVgrp\tE\n'
    """
    print 'data2text', numpy.shape(data)

    writelines = ''
    vlsr_grp = group_vlsr(data, threshold=threshold)
    for cluster in data:
        for epoch, maser_data in enumerate(cluster):
            for line in maser_data:
                vlsr_idx = numpy.argmin(numpy.abs(line[0]-vlsr_grp[:, 0]))
                group = int(vlsr_grp[vlsr_idx, 1])
                line_ = ['{:06f}'.format(val) for val in line[:5]]
                writelines += '\t'.join(line_)
                writelines += '\t{}'.format(group)
                writelines += '\t{}\n'.format(epoch)
        writelines += '\n'
    return writelines


def main(args):
    # read data file with maser astrometry observations
    astrometry_epoch_data = list(chain.from_iterable(args.infile))
    if args.refine and len(astrometry_epoch_data) > 1:
        raise RuntimeError('You can only refine one input file at a time')

    # get observed maser spots measured over epochs
    astrometry_data = []
    # assume at least one epoch worth of data
    for filename in astrometry_epoch_data:
        # refine spot selection of previous cluster
        if args.refine:
            astrometry_data = read_previous(filename)
        else:
            # Vr, X, Xerr, Y, Yerr
            maser_spots = read_file(filename,
                                    x_range=args.ra_range,
                                    y_range=args.decl_range)
            astrometry_data.append(maser_spots)

    # ensure input data is packaged correction with dims [#clusters, #epoch, [data]]
    if len(numpy.shape(astrometry_data)) < 2:
        data = [astrometry_data]
    else:
        data = astrometry_data

    # show data to user for measurement selection
    selected_maser_clusters = show_clusters(
        data,
        clr_idx=0)  # colorbar over vlsr

    # if no output file, only display the masers for inspection
    if args.outfile is None:
        quit()

    # recover selected points from epoch data
    with open(args.outfile, 'w') as fout:
        fout.write('V\tX\tXerr\tY\tYerr\tVgrp\tE\n')

        # keep all data in display if output filename is provided
        if numpy.size(selected_maser_clusters) < 1:
            fout.write(data2text(data,
                                 threshold=args.vlsr_threshold))

        # assume multiple clusters in data
        # a cluster is a selection of points over epoch
        # clusters are separated by coordinates
        for cluster in selected_maser_clusters:
            maser_cluster = associate_selection(
                cluster[:, 0],  # x
                cluster[:, 1],  # y
                astrometry_data)

            maser_cluster_data = [[] for idx in range(len(maser_cluster.keys()))]
            for key in numpy.sort(maser_cluster.keys()):
                epoch = key.split('_')[-1].strip()
                maser_cluster_data[int(epoch)] = maser_cluster[key]
            fout.write(data2text(maser_cluster_data,
                       threshold=args.vlsr_threshold))
            # separate clusters with blank line
            fout.write('\n')


if __name__ == '__main__':
    usage = "%(prog)s [options] -i <input file(s)> -o <output file>"
    description = "Maser spot selection tool for proper motion calculation"
    version = "%(prog)s 0.1"
    parser = argparse.ArgumentParser(usage=usage, description=description)
    main(cli_(parser, version=version))

# -fin-
