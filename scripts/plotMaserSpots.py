from itertools import chain
from matplotlib.lines import Line2D

import matplotlib
import matplotlib.pyplot as plt
import numpy

from cli import plotMS_cli as cli_
from fileio import read_selected_clusters as read_file


class measurement_display:
    def __init__(self, n_epochs, clr_range=[-1, 1]):
        self.epochs = numpy.arange(n_epochs)
        self.legend_labels = ['epoch {}'.format(epoch) for epoch in self.epochs]
        # set marker per epoch
        self.markers = ['o', '^', '*']
        if n_epochs > 3:
            self.markers = Line2D.filled_markers
        self.marker_idx = (self.epochs % len(self.markers)).astype(int)
        # set color over range
        self.clim = [int(clr_range[0]), int(clr_range[1])+1]
        self.colors = numpy.arange(self.clim[0], self.clim[1])

    # plot all measured maser spots by epoch and allow user selection
    def plot_masers(self, data, clr_idx=0):
        cmap = plt.cm.get_cmap('jet')
        fig = plt.figure(figsize=(13, 11), facecolor='white')
        ax = fig.add_subplot(111)
        norm = plt.Normalize(vmin=self.clim[0], vmax=self.clim[1])
        # display measurements per epoch
        data_x = []
        data_y = []
        for epoch in self.epochs:
            v_ = data[epoch][:, clr_idx]
            x_ = data[epoch][:, 1]
            y_ = data[epoch][:, 3]
            colors = [cmap(norm(value)) for value in v_]
            ax.scatter(x_,
                       y_,
                       c=colors,
                       s=75,
                       edgecolor='none',
                       marker=self.markers[self.marker_idx[epoch]],
                       vmin=self.clim[0],
                       vmax=self.clim[1],
                       cmap=plt.cm.jet,
                       norm=norm,
                       )
            data_x.append(x_)
            data_y.append(y_)
        data = numpy.vstack([list(chain.from_iterable(data_x)),
                             list(chain.from_iterable(data_y))]).T

        # set select points for picking measurements
        data_selected = []

        def on_pick(event):
            ind = event.ind
            data_selected.append(data[ind].flatten())
            fig.canvas.draw()

        ax.scatter(data[:, 0],
                   data[:, 1],
                   c=["black"] * len(data),
                   marker='o',
                   picker=True,
                   alpha=0.01,
                   s=[2] * len(data))

        ax.set_aspect('auto')
        ax.invert_xaxis()
        ax.set_xlabel('RA offset [arcsec]')
        ax.set_ylabel('Dec offset [arcsec]')
        fig.canvas.mpl_connect('pick_event', on_pick)
        ax.legend(self.legend_labels,
                  loc='upper center',
                  prop={'size': 10},
                  bbox_to_anchor=(0.5, -0.06),
                  ncol=len(self.epochs))
        cax, _ = matplotlib.colorbar.make_axes(ax)
        matplotlib.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm)
        plt.show()

        data_selected = list(chain.from_iterable(data_selected))
        data_selected = numpy.asarray(data_selected).reshape(len(data_selected)/2, 2)
        return data_selected

    # show calculated proper motion per cluster
    def pmotion(self, data, clr_idx=0, centroid=False, epoch_pm=False):
        cmap = plt.cm.get_cmap('jet')
        fig = plt.figure(figsize=(13, 11), facecolor='white')
        ax = fig.add_subplot(111)
        norm = plt.Normalize(vmin=self.clim[0], vmax=self.clim[1])
        ax.hold(True)

        # plot maser data spots
        for epoch, masers in enumerate(data['masers']):
            masers = numpy.asarray(masers)
            v_ = masers[:, clr_idx]
            x_ = masers[:, 1]
            y_ = masers[:, 3]
            if len(x_) < 1:
                continue
            colors = [cmap(norm(value)) for value in v_]
            ax.scatter(x_,
                       y_,
                       c=colors,
                       s=50,
                       marker=self.markers[self.marker_idx[epoch]],
                       vmin=self.clim[0],
                       vmax=self.clim[1],
                       cmap=plt.cm.jet,
                       norm=norm,
                       )
        # plot pm per epoch if you have it
        for epoch, pm in enumerate(data['pm']):
            for spot_pm in pm:
                group = spot_pm[0]
                masers = numpy.array(data['masers'][epoch], dtype=float)
                grp_mask = (masers[:, -1] == float(group))
                value = masers[grp_mask, clr_idx].mean()
                if len(spot_pm) > 3:
                    [x0, y0] = numpy.array(spot_pm[1:3], dtype=float)
                    propm = numpy.array(spot_pm[-2:], dtype=float)  # arcsec
                    [mu_x, mu_y] = propm*1e3  # mas
                    [epoch0, epoch1] = numpy.array(spot_pm[3].split('-'), dtype=int)-1

                    for line_ in data['pm'][epoch1]:
                        if line_[0] == spot_pm[0]:
                            [x1, y1] = numpy.array(line_[1:3], dtype=float)
                            break

                    if centroid:
                        ax.scatter(x0, y0,
                                   marker=self.markers[self.marker_idx[epoch0]],
                                   color=cmap(norm(value)),
                                   alpha=0.1,
                                   s=100)
                        ax.scatter(x1, y1,
                                   marker=self.markers[self.marker_idx[epoch1]],
                                   color='k',
                                   alpha=0.1,
                                   s=100)
                    ax.arrow(x0, y0,
                             x1-x0, y1-y0,
                             width=1e-7, length_includes_head=True,
                             head_width=0.00005, head_length=0.0001,
                             fc='k', ec='k',
                             alpha=0.3,
                             )

                    if epoch_pm:
                        plt.annotate((r'($\mu_\alpha$, $\mu_\delta$) = (%.2f, %.2f) mas/yr' %
                                     (mu_x, mu_y)),
                                     xy=(x1, y1),
                                     xycoords='data',
                                     textcoords='offset points')

        ax.set_aspect('auto')
        ax.invert_xaxis()
        ax.set_xlabel('RA offset [arcsec]')
        ax.set_ylabel('Dec offset [arcsec]')
        ax.legend(self.legend_labels,
                  loc='upper center',
                  prop={'size': 10},
                  bbox_to_anchor=(0.5, -0.06),
                  ncol=len(self.epochs))
        cax, _ = matplotlib.colorbar.make_axes(ax)
        matplotlib.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm)


# assume data input [#epochs, [Vr, X, Xerr, Y, Yerr, Vr_grp]]
def find_clrbar_range(data, col_idx):
    val_min = numpy.inf
    val_max = -numpy.inf
    for cntr in range(len(data)):
        data[cntr] = numpy.asarray(data[cntr])
        values = data[cntr][:, col_idx]
        val_min = numpy.min((val_min, values.min()))
        val_max = numpy.max((val_max, values.max()))
    return [val_min, val_max]


def show_clusters(data, clr_idx):
    for maser_data in data:
        n_epochs = len(maser_data)
        clr_range = find_clrbar_range(maser_data, clr_idx)
        pmplot = measurement_display(n_epochs,
                                     clr_range=clr_range)
        # plot and select maser positions per epoch
        maser_clusters = []
        while True:
            selection_cluster = numpy.asarray(
                pmplot.plot_masers(maser_data, clr_idx=clr_idx))
            # Else accumulate all selected maser spots
            if not selection_cluster.size:
                break
            maser_clusters.append(selection_cluster)
    return maser_clusters


if __name__ == '__main__':
    import argparse
    usage = "%(prog)s [options] -i <input file>"
    description = "Show maser clusters selected for PM calculation"
    version = "%(prog)s 0.1"
    parser = argparse.ArgumentParser(usage=usage, description=description)
    args = cli_(parser, version=version)

    # read data for selected maser spots
    data = read_file(args.infile)

    # vlsr defined colorbar
    clr_idx = 0
    # color by group
    if args.groups:
        clr_idx = -1
    show_clusters(data, clr_idx)

# -fin-
