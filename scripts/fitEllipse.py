from astropy.coordinates import Angle
from numpy.linalg import eig, inv
import matplotlib.patches as mpatches
import matplotlib.pylab as plt
import numpy as np
import math


def easyEllipse(x, y):
    # ellipse center
    [x0, y0] = [np.mean(x), np.mean(y)]
    # ellipse angle of rotation
    m, c = np.polyfit(x, y, 1)
    # ellipse axis length
    ny = x * m + c
    theta = math.atan2(ny[-1]-ny[0], x[-1]-x[0])
    # a length of the fitted line
    a = np.sqrt((x[-1]-x[0])**2 + (ny[-1]-ny[0])**2)
    # b distance to farthest point from the line
    b = np.abs(y-ny).max()
    return [x0, y0], [a, b], theta


def fitEllipse(x, y):
    x = x[:, np.newaxis]
    y = y[:, np.newaxis]
    D = np.hstack((x*x, x*y, y*y, x, y, np.ones_like(x)))
    S = np.dot(D.T, D)
    C = np.zeros([6, 6])
    C[0, 2] = C[2, 0] = 2
    C[1, 1] = -1
    E, V = eig(np.dot(inv(S), C))
    n = np.argmax(np.abs(E))
    a = V[:, n]
    return a


def ellipse_center(a):
    b, c, d, f, g, a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    num = b*b-a*c
    x0 = (c*d-b*f)/num
    y0 = (a*f-b*d)/num
    return np.array([x0, y0])


def ellipse_angle_of_rotation(a):
    b, c, d, f, g, a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    if b == 0:
        if a > c:
            return 0
        else:
            return np.pi/2
    else:
        if a > c:
            return np.arctan(2*b/(a-c))/2
        else:
            return np.pi/2 + np.arctan(2*b/(a-c))/2


def ellipse_axis_length(a):
    b, c, d, f, g, a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    up = 2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g)
    down1 = (b*b-a*c)*((c-a)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    down2 = (b*b-a*c)*((a-c)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    res1 = np.sqrt(up/down1)
    res2 = np.sqrt(up/down2)
    return np.array([res1, res2])


if __name__ == '__main__':

    x = np.array([-0.57226, -0.57227, -0.57228, -0.57236])
    y = np.array([5.40514, 5.40523, 5.40515, 5.40511])

    # ellipse fit
    a = fitEllipse(x, y)
    center = ellipse_center(a)
    phi = ellipse_angle_of_rotation(a)
    axes = ellipse_axis_length(a)

    print "center = ",  center
    print "angle of rotation = ",  phi
    print "axes = ", axes

    fig, ax = plt.subplots(1, 1)
    ax.hold(True)
    ax.plot(x, y, 'ko')
    x0, y0 = center[0], center[1]
    a, b = axes[0], axes[1]
    theta = Angle(phi, 'rad')
    clr = 'red'
    e = mpatches.Ellipse((x0, y0),
                         2*a, 2*b,
                         theta.degree,
                         edgecolor=clr, facecolor='none')
    ax.scatter(center[0], center[1], marker='.', color=clr, s=100)
    ax.add_patch(e)
    plt.axis('tight')
    plt.show()

# -fin-
