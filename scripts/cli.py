# Constains command line input options for all maser PM calculation scripts


# command line interface parameters for selectMaserSpots
def selectMS_cli(parser, version='v0.1'):
    parser.add_argument(
        '--version',
        action='version',
        version=version)
    parser.add_argument(
        '-i',
        '--infile',
        default=[],
        type=str,
        action='append',
        nargs='+',
        required=True,
        help='\
Maser motion measurements (**required**)')
    parser.add_argument(
        '-o',
        '--outfile',
        type=str,
        help='\
File to save clusters of selected maser spots')
    parser.add_argument(
        '--ra-range',
        type=float,
        nargs=2,
        metavar=('MIN_RA', 'MAX_RA'),
        help='\
Minimum and maximum RA offset of cluster to select')
    parser.add_argument(
        '--decl-range',
        type=float,
        nargs=2,
        metavar=('MIN_DECL', 'MAX_DECL'),
        help='\
Minimum and maximum Declination of selected cluster')
    parser.add_argument(
        '--vlsr-threshold',
        type=float,
        default=0.1,
        help='\
Grouping maser clusters by Vlsr within +- threshold')
    parser.add_argument(
        '--refine',
        action='store_true',
        help='\
View already selected maser clusters to refine spot selection, \
reading from previously generated data cluster files.')
    return parser.parse_args()


# command line interface parameters for plotMaserSpots
def plotMS_cli(parser, version='v0.1'):
    parser.add_argument(
        '--version',
        action='version',
        version=version)
    parser.add_argument(
        '-i',
        '--infile',
        type=str,
        required=True,
        help='\
Maser cluster file (**required**)')
    parser.add_argument(
        '--groups',
        action='store_true',
        help='\
Show maser spots colored by associated vlsr groups. \
Default is simply to color based on vlsr')
    return parser.parse_args()


# command line interface parameters for pmMaserSpots
def pmMS_cli(parser, version='v0.1'):
    parser.add_argument(
        '--version',
        action='version',
        version=version)
    parser.add_argument(
        '-i',
        '--infile',
        type=str,
        required=True,
        help='\
Maser cluster spots locations (**required**)')
    parser.add_argument(
        '-p',
        '--parameters',
        type=str,
        required=True,
        help='\
Parameter file with observation metadata \
required for proper motion calculation (**required**)')
    parser.add_argument(
        '-o',
        '--outfile',
        type=str,
        required=True,
        help='\
Output file for calculated proper motion (**required**)')
    parser.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        help='\
Display proper motion resulting graph')
    return parser.parse_args()


# command line interface parameters for inspectMaserSpots
def inspectMS_cli(parser, version='v0.1'):
    parser.add_argument(
        '--version',
        action='version',
        version=version)
    parser.add_argument(
        '-i',
        '--infile',
        type=str,
        required=True,
        help='\
Maser PM calculation data (**required**)')
    parser.add_argument(
        '--centroid',
        action='store_true',
        help='\
Display centroid from ellipse fitting for pm calculation')
    parser.add_argument(
        '--pm',
        action='store_true',
        help='\
Display pm calculation per epoch')
    return parser.parse_args()

# -fin-
