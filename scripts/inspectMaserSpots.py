import argparse
import matplotlib.pyplot as plt

from cli import inspectMS_cli as cli_
from fileio import read_relative_pm as read_file
from plotMaserSpots import measurement_display


def main(args):

    # unpack maser pm data
    clusters, grp_range, vlsr_range = read_file(args.infile)
    for idx, cluster in enumerate(clusters):
        pmplot = measurement_display(len(cluster['masers']),
                                     clr_range=vlsr_range)
        pmplot.pmotion(cluster,
                       centroid=args.centroid,
                       epoch_pm=args.pm,
                       )
    plt.show()


if __name__ == '__main__':
    usage = "%(prog)s [options] -i <input file(s)> -p <metadata>"
    description = "Maser spot proper motion inspection tool"
    version = "%(prog)s 0.1"
    parser = argparse.ArgumentParser(usage=usage, description=description)
    main(cli_(parser, version=version))

# -fin-
