import argparse
import ephem
import numpy

from datetime import datetime
from cli import pmMS_cli as cli_
from fileio import read_selected_clusters as read_file
from fileio import read_yaml
from fileio import write_relative_pm as write_file
from fitEllipse import (fitEllipse,
                        easyEllipse,
                        ellipse_center,
                        ellipse_angle_of_rotation,
                        ellipse_axis_length)


def extend_array(array_, len_):
    for add_el in range(len_-len(array_)):
        array_.extend([0])
    return array_


# fit ellipse to calculate centroid, and orientation
def fit_ellipse(x, y, verbose=False, fancy=False):
    sort_idx = x.argsort()
    x = x[sort_idx[::-1]]
    y = y[sort_idx[::-1]]
    if fancy:
        a = fitEllipse(x, y)
        center = ellipse_center(a)
        phi = ellipse_angle_of_rotation(a)
        axes = ellipse_axis_length(a)
    else:
        center, axes, phi = easyEllipse(x, y)
    if verbose:
        print('\tcenter = {}'.format(center))
        print('\tangle of rotation = {}'.format(phi))
        print('\taxes = {}'.format(axes))
    return [center, phi, axes]


# calculation relative proper motion
def pmotion(ra, decl, epoch_jd):
    rel_pm_x = numpy.diff(ra)/numpy.diff(epoch_jd)
    rel_pm_y = numpy.diff(decl)/numpy.diff(epoch_jd)
    orientation = numpy.arctan2(rel_pm_y, rel_pm_x)
    return rel_pm_x, rel_pm_y, orientation


def main(args):
    # per epoch meta data for calculations
    parameters = read_yaml(args.parameters)
    epoch_data = parameters['dataset']
    nepochs = len(epoch_data)

    epoch_jd = []
    origin_correction = []
    for cnt, params in enumerate(parameters['dataset']):
        origin_correction.append(numpy.array(params['offset'].strip().split(','), dtype=float))
        date = ephem.Date(datetime.strptime(params['datetime'], '%Y-%m-%d %H:%M'))
        epoch_jd.append(ephem.julian_date(date))
    epoch_jd = numpy.asarray(epoch_jd)  # days
    epoch_jd = epoch_jd/365.25  # years
    origin_correction = numpy.asarray(origin_correction)

    # unpack input maser data
    # calculate ellipse fit per epoch for each cluster
    data = read_file(args.infile)
    for idx, cluster in enumerate(data):
        pm_calc_info = []  # [group, epoch, [center]]
        for epoch, masers in enumerate(cluster):
            masers = numpy.asarray(masers, dtype=float)
            [nspots, nvals] = numpy.shape(masers)
            group_centers = numpy.empty((nspots, 2))
            # unpack per epoch data
            for group in numpy.unique(masers[:, -1]):
                grp_mask = (masers[:, -1] == group)
                group_idx = int(group)
                x = masers[grp_mask, 1]
                y = masers[grp_mask, 3]
                # fit ellipses per epoch to calculate centroid
                if args.verbose:
                    print('Fit ellipse for cluster {}, epoch {}, group {}'.format(
                        idx, epoch, int(group)))
                if grp_mask.sum() > 2:
                    [center, phi, axes] = fit_ellipse(x, y, verbose=args.verbose)
                elif grp_mask.sum() > 1:
                    center = [numpy.mean(x), numpy.mean(y)]
                    if args.verbose:
                        print('\tcenter = {}'.format(center))
                else:
                    center = numpy.hstack([x[0], y[0]])
                    if args.verbose:
                        print('\tcenter = {}'.format(center))

                # extend array if the array is to short
                if group_idx >= len(pm_calc_info):
                    pm_calc_info = extend_array(pm_calc_info, group_idx+1)
                # need a centroid per epoch
                if type(pm_calc_info[group_idx]) is not list:
                    pm_calc_info[group_idx] = [[] for cntr in range(nepochs)]
                # add calculated centroid for pm
                pm_calc_info[group_idx][epoch] = center
                group_centers[grp_mask] = center

        # for each cluster - calculate proper motion per group over epochs
        ngroups = len(pm_calc_info)
        for group in range(ngroups):
            ra = []
            decl = []
            dt = []
            if not isinstance(pm_calc_info[group], list):
                continue
            for epoch, centroid in enumerate(pm_calc_info[group]):
                if len(centroid) > 0:
                    ra.append(centroid[0])  # arcsec
                    decl.append(centroid[1])  # arcsec
                    dt.append(epoch_jd[epoch])  # year
            if len(dt) > 1:  # min len for pm
                group_maser_obs = numpy.nonzero([(len(elt) > 0) for elt in pm_calc_info[group]])[0]
                pm_x, pm_y, orientation = pmotion(ra, decl, dt)
                for pm_idx, epoch_idx in enumerate(group_maser_obs[:-1]):
                    pm_epochs = '{}-{}'.format(epoch_idx+1, group_maser_obs[pm_idx+1:][0]+1)
                    pm_calc_info[group][epoch_idx] = numpy.hstack([pm_calc_info[group][epoch_idx], [pm_epochs, pm_x[pm_idx], pm_y[pm_idx]]])

        # Add relative pm to maser pm data
        # [..., X0, Y0, ejd0-ejd1, mu_x, mu_y]
        for epoch, masers in enumerate(cluster):
            masers = numpy.array(masers)
            [nspots, nvals] = masers.shape
            masers = numpy.hstack((masers,
                                   numpy.array([None]*nspots)[..., numpy.newaxis]))
            for spot in masers:
                group_idx = int(spot[-2])
                spot[-1] = pm_calc_info[group_idx][epoch]
            data[idx][epoch] = masers

    # cluster data matrix now contains
    # [#clusters, #epoch, [V X dX Y dY Vgrp X0 Y0 meta MuX MuY]
    write_file(args.outfile, data)


if __name__ == '__main__':
    usage = "%(prog)s [options] -i <input file(s)> -p <metadata>"
    description = "Maser spot proper motion calculator"
    version = "%(prog)s 0.1"
    parser = argparse.ArgumentParser(usage=usage, description=description)
    main(cli_(parser, version=version))

# -fin-
