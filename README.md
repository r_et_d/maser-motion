# Maser Motion

Maser proper motion from observation epoch data

## Select clusters of maser spots visually
Display observation data over epochs allowing users to zoom and interactively
select maser spots.

*  Using the online display, zoom and inspect maser cluster regions
*  Once a cluster of interest is identified:
    *  select maser spots by clicking on the data points plotted,
    *  the selection must represent a cluster of maser spots observed over two or more epochs,
    *  relative proper motion will be calculated per cluster
* End the cluster selection by closing the figure.
* A new figure will open, zoom and select the next cluster.
* The cycle of display and selection will continue until the user have not
  selected anymore data points. This is achieved by simply close the figure without selection.

Example implementation   
``
python selectMaserSpots.py -i ../data/epoch-kava-1.peak ../data/epoch-kava-2.peak ../data/epoch-kava-3.peak -o maser_spots.tsv
``

Note:   
Input filenames are read in order as provided when the script is called.   
The observational data for each maser spot selected, will be saved to output file for further processing.


## Calculate proper motion for selected maser spot clusters
Read in selected clusters from file   
Additional parameters needed for proper motion calculation, such as observation date, is specified in a parameter file   
Input data for each cluster will be augmented with the following information

*  Ellipse fit parameters for calculating the relative proper motion between epochs
*  Calculated proper motion of the cluster

Output file contains the following data per line

*  Columns 0 to 4: Spot information V, X, dX, Y, dY
*  Columns 5 to 9: Epoch information x0, y0, a, b, phi
*  Columns 10 to 14: Cluster information muX, muY, Epoch, m, c, theta

Column parameters

*  V: observed radial velocity [km/s]
*  X, dX : maser spot normalised RA location and measurement err [arcsec]
*  Y, dY : maser spot normalised Decl location and measurement err [arcsec]
*  x0, y0 : epoch centroid from ellipse fit
*  a, b : fitted ellipse short and long axes
*  angle : ellipse rotation angle
*  muX, muY : relative proper motion over epochs
*  Epoch : observation epoch of data line
*  m, c : line fit during proper motion calculation
*  theta : fitted line angle.

Example implementation   
``
python pmMaserSpots.py -i maser_spots.tsv -p ../config/pmotion_kava.yaml -o maser_spots_pm.tsv -v
``

## Display proper motion for visual inspection
Given the underlying assumption of consistent motion in the same direction, proper motion over all epochs are calculated.
Additional displays to verify this assumption, is the option to display

*  Ellipse fit per epoch to obtain centroid with `-e` option
*  Proper motion between individual epochs by adding the `--pm` option

Example implementation   
``
python inspectMaserSpots.py -i maser_spots_pm.tsv -e --pm
``

