#! /bin/bash

python selectMaserSpots.py -i ../data/* --ra-range -0.49 -0.465 --decl-range 5.310 5.340 -o maser_cluster_1.tsv --vlsr-threshold 0.2
python selectMaserSpots.py -i ../data/* --ra-range -0.625 -0.559 --decl-range 5.383 5.389  -o maser_cluster_2.tsv --vlsr-threshold 0.2
python selectMaserSpots.py -i ../data/* --ra-range -0.5725 -0.5710 --decl-range 5.404 5.409  -o maser_cluster_3.tsv --vlsr-threshold 0.2
python selectMaserSpots.py -i ../data/* --ra-range -0.683 -0.677 --decl-range 5.326 5.340  -o maser_cluster_4.tsv --vlsr-threshold 0.2
python selectMaserSpots.py -i ../data/* --ra-range -0.692 -0.680 --decl-range 5.285 5.313  -o maser_cluster_5.tsv --vlsr-threshold 0.2
python selectMaserSpots.py -i ../data/* --ra-range -0.665 -0.659 --decl-range 5.316 5.319  -o maser_cluster_6.tsv --vlsr-threshold 0.2

for file in *.tsv
do
    python plotMaserSpots.py -i $file
done

for file in *.tsv
do
    python pmMaserSpots.py -i $file -p ../config/pmotion_kava.yaml -o pm_$file -v
done

for file in pm_maser_cluster_*
do
    python inspectMaserSpots.py -i $file
done

